<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Imeiautorizado;
use App\Models\Funcionario;
use App\Models\Casilla;
use App\Models\Eleccion;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF; //--- Se agregó esta línea

class ImeiautorizadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sql="SELECT ia.id, f.nombrecompleto as funcionario, c.ubicacion as casilla,  e.periodo as eleccion, ia.imei
        FROM imeiautorizado ia 
        INNER JOIN funcionario f ON ia.funcionario_id = f.id
        INNER JOIN eleccion e ON ia.eleccion_id = e.id
        INNER JOIN casilla c ON ia.casilla_id = c.id";

        $imeiautorizados = DB::select($sql);
        return view("imeiautorizado/list",
            compact("imeiautorizados")); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        $funcionarios = Funcionario::all();
        $elecciones = Eleccion::all();
        $casillas = Casilla::all();

        return view("imeiautorizado/create",
            compact("funcionarios","casillas","elecciones"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'funcionario_id'=>'required|integer',
            'casilla_id'=>'required|integer',
            'eleccion_id'=>'required|integer',
            'imei'=>'required|max:200'
        ]);

        $data = [
            "funcionario_id" => $request->funcionario_id ,
            "casilla_id" => $request->casilla_id,
            "eleccion_id"=>$request->eleccion_id,
            "imei"=> $request->imei
        ];

        Imeiautorizado::create($data);
        return redirect('imeiautorizado')->with('success',
            ' guardado satisfactoriamente ...');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $funcionarios = Funcionario::all();
        $elecciones = Eleccion::all();
        $casillas = Casilla::all();
        $imeiautorizados = Imeiautorizado::find($id);
        
        return view('imeiautorizado/edit',compact("imeiautorizados","elecciones","casillas","funcionarios"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     $request->validate([
            'funcionario_id'=>'required|integer',
            'eleccion_id'=>'required|integer',
            'casilla_id'=>'required|integer',
            'imei'=>'required|max:200'
        ]);


        $data = [
            "funcionario_id" => $request->funcionario_id ,
            "eleccion_id" => $request->eleccion_id,
            "casilla_id" => $request->casilla_id,
            "imei"=>$request->imei
        ];

        Imeiautorizado::whereId($id)->update($data);
        return redirect('imeiautorizado')
        ->with('success', 'Actualizado correctamente...');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $imeiautorizado = Imeiautorizado::find($id);
        $imeiautorizado->delete();
        return redirect('imeiautorizado');
    }

    public function generatepdf()
    {
         $sql="SELECT ia.id, f.nombrecompleto as funcionario, c.ubicacion as casilla,  e.periodo as eleccion, ia.imei
        FROM imeiautorizado ia 
        INNER JOIN funcionario f ON ia.funcionario_id = f.id
        INNER JOIN eleccion e ON ia.eleccion_id = e.id
        INNER JOIN casilla c ON ia.casilla_id = c.id";
  
        $imeiautorizados = DB::select($sql);
      
        $pdf = PDF::loadView('imeiautorizado/vista', ['imeiautorizados'=>$imeiautorizados]);
        return $pdf->stream('imeiautorizado.pdf');
    }


}
