<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Candidato;
use App\Models\Voto;
use App\Models\Votocandidato;
use Barryvdh\DomPDF\Facade as PDF; //--- Se agregó esta línea
use Illuminate\Support\Facades\DB;

class VotocandidatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $sql = "SELECT  v.id as id, e.periodo,ca.ubicacion as casilla, c.nombrecompleto as candidato, vc.votos
                from votocandidato vc
                inner join voto v on vc.voto_id = v.id
                inner join eleccion e on v.eleccion_id = e.id
                inner join casilla ca on v.casilla_id = ca.id
                inner join candidato c on vc.candidato_id = c.id 
                order by id"; 

        $votocandidatos = DB::select($sql);
        return view("votocandidato/list",compact("votocandidatos")); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generatepdf()
    {
      /*$sql="  SELECT v.id as voto, c.nombrecompleto as candidato,  vc.votos
        FROM votocandidato vc 
        INNER JOIN voto v ON vc.voto_id = v.id
        INNER JOIN candidato c ON vc.candidato_id = c.id
        ORDER BY voto";*/ 
        $sql = "SELECT  v.id as id, e.periodo,ca.ubicacion as casilla, c.nombrecompleto as candidato, vc.votos
                from votocandidato vc
                inner join voto v on vc.voto_id = v.id
                inner join eleccion e on v.eleccion_id = e.id
                inner join casilla ca on v.casilla_id = ca.id
                inner join candidato c on vc.candidato_id = c.id 
                order by id"; 

        $votocandidatos = DB::select($sql);
      // print_r($casillas);
        $pdf = PDF::loadView('votocandidato/vista', ['votocandidatos'=>$votocandidatos]);
        return $pdf->stream('votocandidato.pdf');
    }

    public function generatechart()
    {
       // $sql = "SELECT candidato_id , votos FROM votocandidato GROUP BY candidato_id";
       // $sql ="SELECT votos,candidato_id FROM votocandidato";

      /*  $sql = "SELECT c.nombrecompleto AS nombre, votos from votocandidato v 
                INNER JOIN candidato c ON c.id= v.candidato_id";*/

          $sql = "SELECT c.nombrecompleto AS nombre,sum(votos) as votos 
                  from votocandidato v 
                  inner join candidato c on c.id =v.candidato_id 
                  group by c.nombrecompleto";  
       

        $votocandidatos = DB::select($sql);
        return view("votocandidato/chart",['votocandidatos'=>$votocandidatos]);
     }

     
}
