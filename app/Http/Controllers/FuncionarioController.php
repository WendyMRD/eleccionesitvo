<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Funcionario;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF; //--- Se agregó esta línea

class FuncionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
       $funcionarios = Funcionario::all();
        return view('funcionario/list', compact('funcionarios'));
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('funcionario/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombrecompleto' => 'required|max:200',
            'sexo' =>'required|max:1'
        ]);

        $data = ["id" => $request->id,
                "nombrecompleto" => $request->nombrecompleto,
                 "sexo" => $request->sexo];

        $funcionario = Funcionario::create($data);
        return redirect('funcionario')->with('success',
        $funcionario->nombrecompleto . ' guardado satisfactoriamente ...');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $funcionario = Funcionario::find($id);
        return view('funcionario/edit',compact('funcionario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nombrecompleto' => 'required|max:200',
            'sexo' =>'required|max:1'
        ]);

        $data = ["id" => $request->id,
                "nombrecompleto" => $request->nombrecompleto,
                 "sexo" => $request->sexo];

        Funcionario::whereId($id)->update($data);
        return redirect('funcionario')
        ->with('success', 'Actualizado correctamente...');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $funcionario = Funcionario::find($id);
        $funcionario->delete();
        return redirect('funcionario');
    }

  /*  public function generatepdf()
    {
  
       $funcionarios = Funcionario::all();
      // print_r($casillas);
        $pdf = PDF::loadView('funcionario/list', ['funcionarios'=>$funcionarios]);
        return $pdf->stream('archivo.pdf');
    }*/

    public function generatepdf()
    {
  
       $funcionarios = Funcionario::all();
      // print_r($casillas);
        $pdf = PDF::loadView('funcionario/vista', ['funcionarios'=>$funcionarios]);
        return $pdf->stream('funcionario.pdf');
    }

    public function generatechart()
    {
       // $sql = "SELECT candidato_id , votos FROM votocandidato GROUP BY candidato_id";
       // $sql ="SELECT votos,candidato_id FROM votocandidato";

        $sql = "SELECT sexo, count(*) AS TOTAL FROM funcionario group by sexo;";
       

        $funcionarios = DB::select($sql);
        return view("funcionario/chart",['funcionarios'=>$funcionarios]);
     }

    
}
