@extends('plantilla')
@section('content')
<style>
	.uper {
		margin-top: 40px;
	}
</style>
<br>
<br>
<h1 align="center">VOTO CANDIDATO</h1>
<br>
<div class="uper">
	@if(session()->get('success'))
	<div class="alert alert-success">
		{{ session()->get('success') }}
	</div><br />
	@endif
	<table class="table table-striped">
		<thead>
			<tr>
				<th align="center">ID VOTO</th>
				<th align="center">PERIODO</th>
				<th align="center">CASILLA</th>
				<th align="center">CANDIDATO</th>
				<th align="center">VOTOS</th>
				
			</tr>
		</thead>
		<tbody>
			@foreach($votocandidatos as $votocandidato)
			<tr>
				<td align="center">{{$votocandidato->id}}</td>
				<td>{{$votocandidato->periodo}}</td>
				<td>{{$votocandidato->casilla}}</td>
				<td>{{$votocandidato->candidato}}</td>
				<td align="center">{{$votocandidato->votos}}</td>
			</tr>
		@endforeach
	</tbody>
</table>

</div>
	
@endsection