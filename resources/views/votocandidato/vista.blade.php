@extends('plantilla')
@section('content')
<!DOCTYPE HTML>
<HTML>
<head>
	<div style='text-align:center;'>	
   
		<h1>PDF generado desde etiquetas html:</h1>
		<h2> Voto Candidato</h2>
		<br>
</head>
   
   </div>
   <BODY>

   <table class="table table-striped" align="center" border="1" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th align="center">ID VOTO</th>
				<th align="center">PERIODO</th>
				<th align="center">CASILLA</th>
				<th align="center">CANDIDATO</th>
				<th align="center">VOTOS</th>
				
			</tr>
		</thead>
		<tbody>
			@foreach($votocandidatos as $votocandidato)
			<tr>
				<td align="center">{{$votocandidato->id}}</td>
				<td>{{$votocandidato->periodo}}</td>
				<td>{{$votocandidato->casilla}}</td>
				<td>{{$votocandidato->candidato}}</td>
				<td align="center">{{$votocandidato->votos}}</td>
			</tr>
		@endforeach
	</tbody>
</table>

<div>
	<div style='text-align:center;'>	
   
   		<h3>&copy;Wendy.dev</h3> 
</div>
<script type="text/php">
		if (isset($pdf) ) {
				$pdf->page_script('
				$font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
				$pdf->text(270, 730, "Pagina $PAGE_NUM de $PAGE_COUNT", $font, 10);
				');
		}
</script>
   </BODY>
</HTML>
@endsection