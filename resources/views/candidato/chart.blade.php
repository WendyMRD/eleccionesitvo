@extends('plantilla')
@section('title','Panel de control')
@section('content')

<html>
  <head>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['candidato', 'Total'],
          @foreach ($candidatos as $candidatos)
          ['Sexo:{{$candidatos->sexo}}',{{$candidatos->TOTAL}}],
          @endforeach
        ]);

        var options = {
          title: 'Gráfica de Genero de Candidatos'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="piechart" style="width: 1000px; height: 500px;"></div>
  </body>
</html>

@endsection