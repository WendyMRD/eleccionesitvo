@extends('plantilla')
@section('content')
<style>
	.uper {
		margin-top: 40px;
	}
</style>

<br>
<br>
<h1 align="center">CANDIDATOS</h1>
<br>

<div class="uper">
	@if(session()->get('success'))
	<div class="alert alert-success">
		{{ session()->get('success') }}
	</div><br />
	@endif
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>NOMBRE</th>
				<th>FOTO</th>
				<th>PERFIL</th>
				<th>SEXO</th>
				<th colspan="2">Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($candidatos as $candidato)
			<tr>
				<td>{{$candidato->id}}</td>
				<td>{{$candidato->nombrecompleto}}</td>
				<td><img src="uploads/{{$candidato->foto}}" width="150" height="150" alt="aqui va la foto"></td>
				<td><a href="uploads/{{$candidato->perfil}}">
					<img src="uploads/icon-pdf.svg" alt="pdf" width="150" height="150"></a>
				</td>
				<td>{{$candidato->sexo}}</td>

				<td><a href="{{ route('candidato.edit', $candidato->id)}}"
					class="btn btn-primary">Edit</a></td>
				<td>
					<form action="{{ route('candidato.destroy', $candidato->id)}}" method="post">
						@csrf
						@method('DELETE')
						<button class="btn btn-danger" type="submit"
						onclick="return confirm('Esta seguro de borrar a {{$candidato->nombrecompleto}}')" >Eliminar</button>
					</form>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
<div>
@endsection