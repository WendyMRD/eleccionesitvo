<!DOCTYPE HTML>
<HTML>
<head>
	<div style='text-align:center;'>	
   
		<h1>PDF generado desde etiquetas html:</h1>
		<h2>Funcionario</h2>
		<br>
</head>
   
   </div>
   <BODY>

     <table class="table table-striped" align="center"  width="80%"  border="1" align="center" cellspacing="0">
		<thead>
			<tr>
				<td align="center">ID</td>
				<td align="center">NOMBRE COMPLETO</td>
				<td align="center">SEXO</td>
			</tr>
		</thead>
		<tbody>
			@foreach($funcionarios as $funcionario)
			<tr>
				<td align="center">{{$funcionario->id}}</td>
				<td>{{$funcionario->nombrecompleto}}</td>
				<td align="center">{{$funcionario->sexo}}</td>
			</tr>
		@endforeach
	</tbody>
</table>

<div>
	<div style='text-align:center;'>	
   
   		<h3>&copy;Wendy.dev</h3> 
</div>
<script type="text/php">
		if (isset($pdf) ) {
				$pdf->page_script('
				$font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
				$pdf->text(270, 730, "Pagina $PAGE_NUM de $PAGE_COUNT", $font, 10);
				');
		}
</script>
   </BODY>
</HTML>