<!DOCTYPE HTML>
<HTML>
<head>
	<div style='text-align:center;'>	
		<h1>PDF generado desde etiquetas html:</h1>
		<h2> Casilla</h2><br>
</head>
   </div>
   <BODY>
      <table class="table table-striped" align="center" width="80%"  border="1" align="center" cellspacing="0">
		<thead>
			<tr>
				<td align="center">ID</td>
				<td align="center">UBICACION</td>
				
			</tr>
		</thead>
		<tbody>
			@foreach($casillas as $casilla)
			<tr>
				<td align="center">{{$casilla->id}}</td>
				<td>{{$casilla->ubicacion}}</td>
				
			</tr>
		@endforeach
	</tbody>
</table>
<div>
	<div style='text-align:center;'>	
   		<h3>&copy;Wendy.dev</h3> 
</div>
<script type="text/php">
		if (isset($pdf) ) {
				$pdf->page_script('
				$font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
				$pdf->text(270, 730, "Pagina $PAGE_NUM de $PAGE_COUNT", $font, 10);
				');
		}
</script>
   </BODY>
</HTML>