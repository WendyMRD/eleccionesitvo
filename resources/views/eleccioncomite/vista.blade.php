<!DOCTYPE HTML>
<HTML>
<head>
	<div style='text-align:center;'>	
   
		<h1>PDF generado desde etiquetas html:</h1>
		<h2> Eleccion Comite</h2>
		<br>
</head>
   
   </div>
   <BODY>

    <table class="table table-striped" align="center"  width="90%"  border="1" align="center" cellspacing="0">
		<thead>
			<tr>
				<th align="center">ID</th>
				<th align="center">PERIODO DE ELECCION</th>
				<th align="center">FUNCIONARIO</th>
				<th align="center">ROL</th>
				
			</tr>
		</thead>
		<tbody>
			@foreach($eleccioncomites as $eleccioncomite)
			<tr> 
				<td align="center">{{$eleccioncomite->id}}</td>
				<td>{{$eleccioncomite->eleccion}}</td>
                <td>{{$eleccioncomite->funcionario}}</td>
                <td>{{$eleccioncomite->rol}}</td>
			</tr>
		@endforeach
	</tbody>
</table>

<div>
	<div style='text-align:center;'>	
   
   		<h3>&copy;Wendy.dev</h3> 
</div>
<script type="text/php">
		if (isset($pdf) ) {
				$pdf->page_script('
				$font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
				$pdf->text(270, 730, "Pagina $PAGE_NUM de $PAGE_COUNT", $font, 10);
				');
		}
</script>
   </BODY>
</HTML>