<!DOCTYPE HTML>
<HTML>
<head>
	<div style='text-align:center;'>	
   
		<h1>PDF generado desde etiquetas html:</h1>
		<h2> Funcionario Casilla</h2>
		<br>
</head>
   
   </div>
   <BODY>
<table class="table table-striped" align="center"  width="90%"  border="1" align="center" cellspacing="0">
		<thead>
			<tr>
				<th align="center">ID</th>
				<th align="center">FUNCIONARIO</th>
				<th align="center">CASILLA</th>
				<th align="center">ROL</th>
				<th align="center">ELECCION</th>
			
			</tr>
		</thead>
		<tbody>
			@foreach($funcionariocasillas as $funcionariocasilla)
			<tr>
				<td align="center">{{$funcionariocasilla->id}}</td>
				<td>{{$funcionariocasilla->funcionario}}</td>
				<td>{{$funcionariocasilla->casilla}}</td>
                <td>{{$funcionariocasilla->rol}}</td>
                <td>{{$funcionariocasilla->eleccion}}</td>
			</tr>
		@endforeach
	</tbody>
</table>

<div>
	<div style='text-align:center;'>	
   
   		<h3>&copy;Wendy.dev</h3> 
</div>
<script type="text/php">
		if (isset($pdf) ) {
				$pdf->page_script('
				$font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
				$pdf->text(270, 730, "Pagina $PAGE_NUM de $PAGE_COUNT", $font, 10);
				');
		}
</script>
   </BODY>
</HTML>