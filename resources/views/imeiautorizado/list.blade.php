@extends('plantilla')
@section('content')
<style>
	.uper {
		margin-top: 40px;
	}
</style>

<br>
<br>
<h1 align="center">IMEI AUTORIZADO</h1>
<br>

<div class="uper">
	@if(session()->get('success'))
	<div class="alert alert-success">
		{{ session()->get('success') }}
	</div><br />
	@endif
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>FUNCIONARIO</th>
				<th>CASILLA</th>
				<th>ELECCION</th>
				<th>IMEI</th>
				<th colspan="2">Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($imeiautorizados as $imeiautorizado)
			<tr>
				<td>{{$imeiautorizado->id}}</td>
				<td>{{$imeiautorizado->funcionario}}</td>
				<td>{{$imeiautorizado->casilla}}</td>
				<td>{{$imeiautorizado->eleccion}}</td>
				<td>{{$imeiautorizado->imei}}</td>

				<td><a href="{{ route('imeiautorizado.edit', $imeiautorizado->id)}}"
					class="btn btn-primary">Edit</a></td>
				<td>
					<form action="{{ route('imeiautorizado.destroy', $imeiautorizado->id)}}" method="post">
						@csrf
						@method('DELETE')
						<button class="btn btn-danger" type="submit"
						onclick="return confirm('Esta seguro de borrar')" >Del</button>
					</form>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
<div>
@endsection