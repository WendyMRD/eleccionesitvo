@extends('plantilla')
@section('content')
<style>
	.uper {
		margin-top: 40px;
	}
</style>
<div class="card uper">
	<div class="card-header">	Editar IMEI AUTORIZADO </div>
	<div class="card-body">
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div><br />
		@endif
		
		<form method="POST" action="{{ route('imeiautorizado.update', $imeiautorizados->id) }}"
		enctype="multipart/form-data">
		{{ csrf_field() }}
		@method('PUT')


			<div class="form-group">
				@csrf
				<label for="id">ID:</label>
				<input type="text" 
				class="form-control" 
				readonly="true" 
				value="{{$imeiautorizados->id}}"
				name="id"/>
			</div>


		<div class="form-group">
		@csrf
		<label for="funcionario">Funcionario:</label>
		<select name="funcionario_id">
			@foreach ($funcionarios as $funcionario)
			  @php
			      $selected = ($imeiautorizados->funcionario_id==$funcionario->id)?" selected":"";
			  @endphp
			<option value="{{$funcionario->id}}" {{$selected}}>{{$funcionario->nombrecompleto}}</option>
			@endforeach
		</select>
	</div>	

		<div class="form-group">
		@csrf
		<label for="eleccion">Eleccion:</label>
		<select name="eleccion_id">
			@foreach ($elecciones as $eleccion)
			  @php
			      $selected = ($imeiautorizados->eleccion_id==$eleccion->id)?" selected":"";
			  @endphp
			<option value="{{$eleccion->id}}" {{$selected}}>{{$eleccion->periodo}}</option>
			@endforeach
		</select>
	</div>		

	<div class="form-group">
		@csrf
		<label for="casilla">Casilla:</label>
		<select name="casilla_id">
			@foreach ($casillas as $casilla)
			  @php
			      $selected = ($imeiautorizados->casilla_id==$casilla->id)?" selected":"";
			  @endphp
			<option value="{{$casilla->id}}" {{$selected}}>{{$casilla->ubicacion}}</option>
			@endforeach
		</select>
	</div>

		<div class="form-group">
			@csrf
			<label for="imeiautorizado">IMEI:</label>
			<input type="text" value="{{$imeiautorizados->imei}}" class="form-control" name="imei"/>
		</div>

			<button type="submit" class="btn btn-primary">Guardar</button>
		</form>
	</div>
</div>
@endsection