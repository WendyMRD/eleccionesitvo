@extends('plantilla')
@section('content')
<style>
	.uper {
		margin-top: 40px;
	}
</style>
<div class="card uper">
	<div class="card-header">	Editar Eleccion </div>
	<div class="card-body">
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div><br />
	@endif
	<form method="POST" action="{{ route('eleccion.update', $eleccion->id) }}"
		enctype="multipart/form-data">
		{{ csrf_field() }}
		@method('PUT')

		<div class="form-group">
			@csrf
			<label for="id">ID:</label>
			<input type="text" 
			class="form-control" 
			readonly="true" 
			value="{{$eleccion->id}}"
			name="id"/>
		</div>

		<div class="form-group">
			@csrf
			<label for="periodo">Eleccion:</label>
			<input type="text" 
			value="{{$eleccion->periodo}}" 
			class="form-control" 
			name="periodo"/>
		</div>

		<div class="form-group">
			@csrf
			<label for="fecha">Fecha:</label>
			<input type="date" 
			class="form-control"
			step="1" min="1910-01-01" 
			max="2100-12-31" 
			value="<?php echo date("Y-m-d");?>" 
			name="fecha" >
		</div>

          <div class="form-group">
          	@csrf
          	<label for="fechaapertura">Fecha Apertura:</label>
          	<input type="date" 
          	class="form-control"
          	step="1" min="1910-01-01" 
          	max="2100-12-31" 
          	value="<?php echo date("Y-m-d");?>"
          	name="fechaapertura" >
          </div>

          <div class="form-group">
          	@csrf
          	<label for="horaapertura">Hora Apertura:</label>
          	<input type="time" 
          	class="form-control"
          	name="horaapertura">
          </div>

          <div class="form-group">
          	@csrf
          	<label for="fechacierre">Fecha Cierre :</label>
          	<input type="date" 
          	class="form-control"
          	step="1" min="2013-01-01" 
          	max="2100-12-31" 
          	value="<?php echo date("Y-m-d");?>"
          	name="fechacierre" >
          </div>

          <div class="form-group">
          	@csrf
          	<label for="horacierre">Hora Cierre:</label>
          	<input type="time" 
          	class="form-control"
          	name="horacierre">
          </div>

          <div class="form-group">
			@csrf
			<label for="observaciones">Observaciones:</label>
			<input type="text" 
			value="{{$eleccion->observaciones}}" 
			class="form-control" 
			name="observaciones"/>
		 </div>

		<button type="submit" class="btn btn-primary">Guardar</button>
	</form>
</div>
</div>
@endsection