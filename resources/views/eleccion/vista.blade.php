<!DOCTYPE HTML>
<HTML>
<head>
	<div style='text-align:center;'>	
   
		<h1>PDF generado desde etiquetas html:</h1>
		<h2> Elecccion</h2>
		<br>
</head>
   
   </div>
   <BODY>

      <table class="table table-striped" align="center"  width="100%"  border="1" align="center" cellspacing="0">
		<thead>
			<tr>
				<td align="center">ID</td>
				<td align="center">PERIODO</td>
				<td align="center">FECHA</td>
				<td align="center">FECHA DE APERTURA</td>
				<td align="center">HORA DE APERTURA</td>
				<td align="center">FECHA DE CIERRE</td>
				<td align="center">HORA DE CIERRE</td>
				<td align="center">OBSERVACIONES</td>
			</tr>
		</thead>
		<tbody>
			@foreach($elecciones as $eleccion)
			<tr>
				<td>{{$eleccion->id}}</td>
				<td>{{$eleccion->periodo}}</td>
                <td>{{$eleccion->fecha}}</td>
                <td>{{$eleccion->fechaapertura}}</td>
                <td>{{$eleccion->horaapertura}}</td>
                <td>{{$eleccion->fechacierre}}</td>
                <td>{{$eleccion->horacierre}}</td>
                <td>{{$eleccion->observaciones}}</td>
			</tr>
		@endforeach
	</tbody>
</table>

<div>
	<div style='text-align:center;'>	
   
   		<h3>&copy;Wendy.dev</h3> 
</div>
<script type="text/php">
		if (isset($pdf) ) {
				$pdf->page_script('
				$font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
				$pdf->text(270, 730, "Pagina $PAGE_NUM de $PAGE_COUNT", $font, 10);
				');
		}
</script>
   </BODY>
</HTML>