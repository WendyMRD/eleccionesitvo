<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//use App\Http\Controller\Api\CasillaController as CasillaController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Route::resource("casilla",CasillaController::class);
/*Route::resource("candidato","Api\CasillaController");
Route::resource("candidato","Api\CandidatoController");
Route::resource("candidato","Api\FuncionarioController");
Route::resource("candidato","Api\EleccionController");
Route::resource("candidato","Api\VotoController");
Route::resource("candidato","Api\RolController");
Route::resource("candidato","Api\ImeiautorizadoController");
*/