function validate(){
        let valid = true;
        if ($("#periodo").val().trim() ==""){
            alert("La casilla PERIODO no puede quedar en vacío");
            valid = false;
        }
        if ($("#horaapertura").val().trim() ==""){
            alert("La casilla HORA APERTURA no puede quedar en vacío");
            valid = false;
        }
        if ($("#horacierre").val().trim() ==""){
            alert("La casilla HORA CIERRE no puede quedar en vacío");
            valid = false;
        }
        if ($("#observaciones").val().trim() ==""){
            alert("La casilla OBSERVACIONES no puede quedar en vacío");
            valid = false;
        }
        return (valid);
    }