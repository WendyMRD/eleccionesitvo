function validate(){
        let valid = true;
        if ($("#nombrecompleto").val().trim() ==""){
            alert("La casilla NOMBRE no puede quedar vacía");
            valid = false;
        }
        if ($("#foto").val().trim() ==""){
            alert("La casilla FOTO no puede quedar vacía");
            valid = false;
        }
        if ($("#perfil").val().trim() ==""){
            alert("La casilla PERFIL no puede quedar vacía");
            valid = false;
        }
        return (valid);
    }